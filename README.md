# Forkio


## Учасники проекту

### 1. Лаврусь Максим

* Шапка сайту з верхнім меню;
* Секція `People Are Talking About Fork`;
    
### 2. Козаченко Олександр

* Блок `Revolutionary Editor`;
* Секція `Here is what you get`;
* Секція `Fork Subscription Pricing`.

## Використані технології

* HTML/CSS;
* JS;
* SCSS;
* Gulp;
* Git